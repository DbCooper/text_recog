#!/bin/bash

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in

    -l|--lmdb)
    LMDB="$2"
    shift # past argument
    shift # past value
    ;;
    -r|--repostrain)
    REPOSTRAIN="$2"
    shift # past argument
    shift # past value
    ;;
    -v|--virtualenv)
    VITRUALENV="$2"
    shift # past argument
    ;;
    -tr|--trainfile)
    TRAINFILE="$2"
    shift # past argument
    shift # past value
    ;;
    -te|--testfile)
    TESTFILE="$2"
    shift # past argument
    ;;
    -va|--validfile)
    VALIDFILE="$2"
    shift # past argument
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters
set -e


if [[ -n $1 ]]; then
    echo "args:"
    echo " -l   or --lmdb           : Using lmdb (y/n) "
    echo " -r   or --repostrain     : Folder train model "
    echo " -tr  or --trainfile      : Path to data train, content path_image \t text with extension .txt"
    echo " -te  or --testfile       : Path to data test, content path_image \t text with extension .txt"
    echo " -va  or --validfile      : Path to data valid, content path_image \t text with extension .txt"
    echo " -v   or --virtualenv     : Virtual env python"
    exit 1
fi

echo "USING LMDB  = ${LMDB}"
echo "VIRTUAL ENV = ${VIRTUALENV}"
echo "REPOSITORY TRAIN     = ${REPOSTRAIN}"
echo "TRAIN FILE (.txt)    = ${LIBPATH}"
echo "TEST FILE (.txt)     = ${DEFAULT}"
echo "VALID FILE (.txt)    = ${VALIDFILE}"

echo "-------------------------set up environment--------------------------"
echo "Steup 1: Set up virtualenv python"

DIR="./${VIRTUAL}"
if [ -d "$DIR" ]; then
    source "${DIR}/bin/activate"
else
    virtualenv "$VIRTUALENV"
    source "${DIR}/bin/activate"
fi

echo "End step 1"
echo "Step 2: Set up lmdb"

if [[ "$LMDB" = "y" ]]; then
    pip install fire
    python3 create_lmdb_dataset.py --inputPath $(pwd) --gtFile "$TRAINFILE" --outputPath $(pwd)/data/train
    python3 create_lmdb_dataset.py --inputPath $(pwd) --gtFile "$TESTFILE" --outputPath $(pwd)/data/test
    python3 create_lmdb_dataset.py --inputPath $(pwd) --gtFile "$VALIDFILE" --outputPath $(pwd)/data/valid
fi
echo "End step 2"

echo "Step 3: Set up warp-ctc on linux"
cd ~/
git clone -b pytorch_bindings https://github.com/SeanNaren/warp-ctc.git
cd warp-ctc
mkdir build
cd build
cmake ..
make

cd ..
cd pytorch_binding
python setup.py install
echo "End Step 3"

echo "Step 4: Start train"
cd $(REPOSTRAIN)
sh start_train_in_repo.sh